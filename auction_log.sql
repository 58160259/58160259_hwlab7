drop table if exists Auction_log;

create table Auction_log
(

                Action          varchar(10)     not null,
                AUC_ID          int             not null,
                TimeStamp       datetime        not null,
                PROD_ID         int             not null,
                BID            float           not null


               
) Engine=InnoDB;

