Drop trigger if exists triggerbid;

delimiter $$

create trigger triggerbid
        AFTER insert on Auction_log
        for each row

begin
       	 if(NEW.Action = 'create') then
                insert Auction values (NEW.AUC_ID,NEW.TimeStamp,NEW.PROD_ID,NEW.BID);

	end if;	

	if(NEW.Action = 'update')   then
                update Auction set 
		BID = NEW.BID
                where AUC_ID = NEW.AUC_ID;
        end if;

	if(NEW.Action = 'delete') then
		delete from Auction 
		where AUC_ID = NEW.AUC_ID;

	end if;

end $$

delimiter ;
